#include <iostream>
#include <queue>
#include <thread>
#include <mutex>
#include <vector>
#include <condition_variable>
#include "../utils.hpp"

using namespace std;

queue<int> q;
ThreadSafeQueue<int> tsq;

void producer_notsafe()
{
    for (int i = 0 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(10ms);
        q.push(i);
        cout << "produced" << i << endl;
    }
}

void consumer_notsafe(int id)
{
    for (;;) {
        while(q.empty()) {}
        int item = q.front();
        q.pop();
        cout << id << " got " << item << endl;
    }
}

void producer()
{
    for (int i = 0 ; i < 5 ; ++i)
    {
        //this_thread::sleep_for(50ms);
        tsq.push(i);
        cout << "produced " << i << endl;
    }
}

void consumer_nowait(int id)
{
    for (;;) {
        int item;
        while (!tsq.pop_nowait(item))
        {
            cout << id << " still waiting for item" << endl;
            this_thread::sleep_for(1s);
        }
        cout << id << " got " << item << endl;
        this_thread::sleep_for(1s);
    }
}

void consumer_wait(int id)
{
    for (;;) {
        int item;
        tsq.pop(item);
        cout << id << " got " << item << endl;
        this_thread::sleep_for(1s);
    }
}

int main()
{
    vector<ThreadGuard> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer_wait, 1);
    thds.emplace_back(consumer_wait, 2);
    //thds.emplace_back(consumer_wait, 2);

    return 0;
}
