#include <iostream>
#include <random>
#include <thread>
#include <celero/Celero.h>
#include <atomic>
#include <mutex>
#include "../utils.hpp"

CELERO_MAIN

using namespace std;

void single_thread(size_t total, size_t& hits)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(0, 1);
    // virtual dart throws
    size_t local_hits = 0;
    for (size_t i = 0 ; i < total ; ++i)
    {
        double x = dis(gen);
        double y = dis(gen);
        if ( x * x + y * y < 1) ++local_hits;
    }
    hits += local_hits;
}

void multithread_1(size_t total, size_t& hits)
{
    // bad solution
    vector<ThreadGuard> thds;
    int n_of_thds = thread::hardware_concurrency();
    for(int i = 0 ; i < n_of_thds ; ++i)
    {
        thds.emplace_back(single_thread, total/n_of_thds, ref(hits));
    }
}

void multithread_2(size_t total, size_t& hits)
{
    size_t n_of_thds = thread::hardware_concurrency();
    vector<size_t> hits_vector;
    hits_vector.resize(n_of_thds);
    {
        vector<ThreadGuard> thds;
        for(size_t i = 0 ; i < n_of_thds ; ++i)
        {
            thds.emplace_back(single_thread, total/n_of_thds, ref(hits_vector[i]));
        }
    } // all threads had stopped
    hits = accumulate(begin(hits_vector), end(hits_vector), 0uL);
}

void atomic_pi(size_t total)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(0, 1);
    size_t n_of_thds = thread::hardware_concurrency();
    // virtual dart throws
    atomic<size_t> hits = 0;
    {
        vector<ThreadGuard> thds;
        for (size_t i = 0 ; i < n_of_thds ; ++i)
        {
            thds.emplace_back([&](void)->void {
                size_t local_hits = 0;
                for (size_t i = 0 ; i < total/n_of_thds ; ++i)                    
                {
                    double x = dis(gen);
                    double y = dis(gen);
                    if ( x * x + y * y < 1) ++local_hits;
                }
                hits += local_hits;
            });
        }
    }
    //cout << "Pi " << 4*static_cast<double>(hits)/total;
}

void mutex_pi(size_t total)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(0, 1);
    size_t n_of_thds = thread::hardware_concurrency();
    // virtual dart throws
    size_t hits = 0;
    mutex mtx;
    {
        vector<ThreadGuard> thds;
        for (size_t i = 0 ; i < n_of_thds ; ++i)
        {
            thds.emplace_back([&](void)->void {
                size_t local_hits = 0;
                for (size_t i = 0 ; i < total/n_of_thds ; ++i)
                {
                    double x = dis(gen);
                    double y = dis(gen);
                    if ( x * x + y * y < 1)
                        ++local_hits;
                }
                lock_guard lg(mtx);
                hits += local_hits;
            });
        }
    }
    //cout << "Pi " << 4*static_cast<double>(hits)/total;
}

const size_t total = 1'000'000;
const int n_of_samples = 2;
const int n_of_iterations = 5;
double mt1_result = 0;
double mt2_result = 0;


BASELINE(Pi, single_thread, n_of_samples, n_of_iterations)
{
    size_t hits = 0;
    single_thread(total, hits);
    //cout << "Pi = " << 4 * static_cast<double>(hits)/total;
}

BENCHMARK(Pi, multithread_bad, n_of_samples, n_of_iterations)
{
    size_t hits = 0;
    multithread_1(total, hits);
    mt1_result = 4 * static_cast<double>(hits)/total;
}

BENCHMARK(Pi, multithread_separated, n_of_samples, n_of_iterations)
{
    size_t hits = 0;
    multithread_2(total, hits);
    mt2_result = 4 * static_cast<double>(hits)/total;
}

BENCHMARK(Pi, atomic_pi, n_of_samples, n_of_iterations)
{
    atomic_pi(total);
}

BENCHMARK(Pi, mutex_pi, n_of_samples, n_of_iterations)
{
    mutex_pi(total);
}

//BENCHMARK(Pi, results, 1, 1)
//{
//    cout << "Multithread 1 Pi = " << mt1_result << endl;
//    cout << "Multithread 2 Pi = " << mt2_result << endl;
//}
