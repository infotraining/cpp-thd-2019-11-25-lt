#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void can_throw(int id, exception_ptr& eptr)
{
    this_thread::sleep_for(1s);
    cout << id << " id" << endl;
    try {
        if (id == 13)
            throw std::runtime_error("bad luck");
    } catch (std::runtime_error& err) {
        cout << "Err in thread " << err.what() << endl;
        eptr = current_exception();
    }
}

int main()
{
    cout << "Hello World!" << endl;
    exception_ptr eptr;
    thread th(can_throw, 13, ref(eptr));
    th.join();
    try
    {
         if(eptr) rethrow_exception(eptr);
    }
    catch (std::runtime_error& err)
    {
        cout << "Error " << err.what() << endl;
    }
    return 0;
}
