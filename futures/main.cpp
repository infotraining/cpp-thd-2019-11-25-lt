#include <iostream>
#include <thread>
#include <future>

using namespace std;

class Calc
{
    promise<int> p;
public:
    void run()
    {
        this_thread::sleep_for(1s);
        p.set_value(123);
    }

    future<int> get_future()
    {
        return p.get_future();
    }
};

int calculate_something(int a)
{
    cout << "calculating" << endl;
    this_thread::sleep_for(100ms);
    return a * a;
}

int main()
{
    // classic solution
    int result;
    thread th1([&]() { result = calculate_something(10);});
    cout << "Result before join " << result << endl;
    th1.join();
    cout << "Result after join " << result << endl;

    // futures
    future<int> res = async(launch::deferred, calculate_something, 13);
    cout << "After launching calculations" << endl;
    this_thread::sleep_for(1s);
    cout << "Result from future1.get() " << res.get() << endl;

    future<int> res2 = async(launch::async, calculate_something, 13);
    cout << "After launching calculations" << endl;
    this_thread::sleep_for(1s);
    cout << "Result from future2.get() " << res2.get() << endl;

    packaged_task<int(int)> pt(calculate_something); //pt -> void(int)
    future<int> res_pt = pt.get_future();
    thread th(move(pt), 4);
    th.detach();
    cout << "Result from pt " << res_pt.get() << endl;

    Calc c;
    future<int> res_promise = c.get_future();
    thread th_promise(&Calc::run, ref(c));
    th_promise.detach();
    cout << "Result from promise " << res_promise.get() << endl;

    return 0;
}
