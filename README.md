## Multithreading in C++

### Additonal information

#### Doc

* https://infotraining.bitbucket.io/cpp-thd/


#### login and password for VM:

```
dev  /  pwd
```

#### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

#### proxy settings

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

#### vcpkg settings

Add to `.profile`

```
export VCPKG_ROOT="/usr/share/vcpkg" 
export CC="/usr/bin/gcc-9"
export CXX="/usr/bin/g++-9"
```

#### GIT

```
git clone https://bitbucket.org/infotraining/cpp-thd-2019-11-25-lt
```

#### Links

* [git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

* [unique_ptr info](http://stackoverflow.com/questions/8114276/how-do-i-pass-a-unique-ptr-argument-to-a-constructor-or-a-function)

* [memory](http://marek.vavrusa.com/c/memory/2015/02/20/memory/)

* [Codinghorror - Infinite space between words](https://blog.codinghorror.com/the-infinite-space-between-words/)

* [compiler explorer](https://gcc.godbolt.org/)

* [Myths Programmers Believe about CPU Caches](https://software.rajivprab.com/2018/04/29/myths-programmers-believe-about-cpu-caches/)

* [Nicolai Josuttis “The Nightmare of Move Semantics for Trivial Classes”](https://www.youtube.com/watch?v=PNRju6_yn3o)

#### Books

* Discovering Modern C++: An Intensive Course for Scientists, Engineers, and Programmers - Peter Gottschling
* The C++ Standard Library: A Tutorial and Reference, Second Edition -  Nicolai M. Josuttis 

