#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include "../utils.hpp"

using namespace std;

timed_mutex mtx;

void background_worker(int id, std::chrono::milliseconds timeout)
{
    cout << "Worker id " << id << " starting" << endl;

    unique_lock<timed_mutex> lk(mtx, try_to_lock);
    if (!lk.owns_lock())
    {
        do
        {
            cout << "I'm waiting for mutex " << id << endl;
            //this_thread::sleep_for(100ms);
        } while(!lk.try_lock_for(100ms));
    }
    cout << "I, id " << id << " have lock" << endl;
    this_thread::sleep_for(timeout);
}

int main()
{
    vector<ThreadGuard> thds;
    thds.emplace_back(background_worker, 1, 500ms);
    thds.emplace_back(background_worker, 2, 500ms);
    return 0;
}
