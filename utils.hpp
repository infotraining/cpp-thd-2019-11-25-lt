#ifndef UTILS_HPP
#define UTILS_HPP
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <future>

class ThreadGuard
{
    std::thread th_;
public:
    template <typename Callable, typename... Args,
                  typename = ::std::enable_if_t<!::std::is_same_v<::std::decay_t<Callable>, ThreadGuard>>>
    ThreadGuard(Callable&& callable, Args&&... args) :
        th_{std::forward<Callable>(callable), std::forward<Args>(args)...}
    {
    }

    ThreadGuard(const ThreadGuard&) = delete;
    ThreadGuard& operator=(const ThreadGuard&) = delete;
    ThreadGuard(ThreadGuard&&) = default;
    ThreadGuard& operator=(ThreadGuard&&) = default;

    ~ThreadGuard()
    {
        if (th_.joinable())
            th_.join();
    }
};

template<typename T>
class ThreadSafeQueue
{
    std::queue<T> q_;
    std::mutex m_;
    std::condition_variable cv_;
public:
    void push(T item)
    {
        std::lock_guard l(m_);
        q_.push(std::move(item));
        cv_.notify_one();
    }
    bool pop_nowait(T& item)
    {
        std::lock_guard l(m_);
        if(q_.empty()) return false;
        item = std::move(q_.front());
        q_.pop();
        return true;
    }
    std::shared_ptr<T> pop_nowait()
    {
        std::lock_guard l(m_);
        if(q_.empty()) return std::shared_ptr<T>();
        auto res = std::make_shared<T>(std::move(q_.front()));
        q_.pop();
        return res;
    }

    void pop(T& item)
    {
        std::unique_lock l(m_);
        cv_.wait(l, [this] { return !q_.empty();}); // wait releases lock
        // lock is acquired
        item = std::move(q_.front());
        q_.pop();
    }
    std::shared_ptr<T> pop()
    {
        std::unique_lock l(m_);
        cv_.wait(l, [this] { return !q_.empty();}); // wait releases lock
        // lock is acquired
        auto res = std::make_shared<T>(std::move(q_.front()));
        q_.pop();
        return res;
    }
};

using Task = std::function<void()>;

class ThreadPool
{
    ThreadSafeQueue<Task> q_;
    std::vector<ThreadGuard> pool_;

    void worker()
    {
         while(true)
         {
             Task newtask;
             q_.pop(newtask);
             if (!newtask) return; // check for work end
             newtask();
         }
    }

public:

    ThreadPool(int pool_size)
    {
        for (int i = 0 ; i < pool_size ; ++i)
            pool_.emplace_back(&ThreadPool::worker, this);
    }

    ~ThreadPool()
    {
        for (size_t i = 0 ; i < pool_.size() ; ++i)
            q_.push(Task());
    }

    void submit(Task t)
    {
        if (t)
            q_.push(t);
    }

    template <typename Callable>
    auto submit(Callable fun)
    {
        using res_t = decltype (fun());
        // convert to Task type
        auto pt = std::make_shared<std::packaged_task<res_t()>>(fun);
        // get future
        std::future<res_t> fut = pt->get_future();
        // put in queue
        q_.push([pt]() { (*pt)(); });
        // return future
        return fut;
    }
};

#endif // UTILS_HPP
