#include <iostream>
#include <fstream>
#include <vector>
#include <thread>
#include "../utils.hpp"

using namespace std;

class Logger
{
    ofstream fout_;
public:
    Logger(const string& fname)
    {
        fout_.open(fname);
    }
    void log(const string& message)
    {
        // slow function
        fout_ << message << endl;
    }
};

class Logger_AO
{
    ofstream fout_;
    ThreadPool ao_{1};

public:
    Logger_AO(const string& fname)
    {
        fout_.open(fname);
    }
    void log(const string& message)
    {
        // use dispatcher
        ao_.submit( [this, message] () {
            fout_ << message << endl;
        });
    }
};


int main()
{
    Logger l("log.txt");
    for (int i = 0 ; i < 10000 ; ++i)
        l.log("Log# " + to_string(i));

    Logger_AO lao("log_AO.txt");
    for (int i = 0 ; i < 10000 ; ++i)
        lao.log("Log# " + to_string(i));

    return 0;
}
