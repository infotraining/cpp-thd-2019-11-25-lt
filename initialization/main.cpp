#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

class Service
{
public:
    virtual void run() = 0;
    virtual ~Service() {};
};

class RealService : public Service
{
public:
    RealService(const string& name) {
        cout << "Creating real service" << endl;
        this_thread::sleep_for(1s);
    }
    void run() override
    {
        cout << "RealService::run()" << endl;
    }
};

class ProxyService : public Service
{
    unique_ptr<RealService> real_service_;
    string name_;
    mutex mtx_;
    once_flag f_;
public:
    ProxyService(const string& name) :
        name_(name), real_service_(nullptr)
    {
    }

    void run_wrong()
    {
        // wrong solution
        if (real_service_ == nullptr)
        {
            lock_guard l(mtx_);
            if (real_service_ == nullptr)
                real_service_ = make_unique<RealService>(name_);
        }
        real_service_->run();
    }

    void run() override
    {
        call_once(f_, [this](){ real_service_ = make_unique<RealService>(name_);});
        real_service_->run();
    }
};

class ProxyStatic : public Service
{
    string name_;
public:
    ProxyStatic(const string& name) : name_(name) {}
    void run() override
    {
        static RealService real_service(name_);
        real_service.run();
    }
};

int main()
{
    cout << "Hello World!" << endl;
    ProxyService service("nazwa");
    service.run();
    service.run();
    ProxyStatic serv2("nazwa");
    serv2.run();
    serv2.run();
    return 0;
}
