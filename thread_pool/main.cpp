#include <iostream>
#include "../utils.hpp"

using namespace std;

void test_fun()
{
    cout << "test fun" << endl;
}

void test_fun_param(int id)
{
    cout << "Test fun id " << id << endl;
}

int some_fun_with_return()
{
    return 256;
}

int main()
{
    ThreadPool tp(4);
    tp.submit(test_fun);
    tp.submit([]() { test_fun_param(42); });
    auto ptr = make_unique<double>(42);
    // doesn't work - std::function does not support movable lambdas
    //tp.submit( [ ptr{move(ptr)} ]() { cout << *ptr << endl;});
    // dangling referene - crashes - so - does not work
    //tp.submit( [ &ptr ]() { cout << *ptr << endl;});
    auto sptr = make_shared<double>(42);
    tp.submit( [ sptr ]() { cout << *sptr << endl;});
    // tp with futures
    future<int> res = tp.submit(some_fun_with_return);
    cout << res.get() << endl;
    return 0;
}
