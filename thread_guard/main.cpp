#include <iostream>
#include <thread>

using namespace std;

class NewClass
{
public:
    NewClass() {}
    ~NewClass()  {}

    NewClass(const NewClass&) = delete; // cpy ctor
    NewClass& operator=(const NewClass&) = delete;  // cpy assignment

    NewClass(NewClass&& r_value) = default; // move ctor
    NewClass& operator=(NewClass&&) = default; // move assignment
};

class ThreadGuard
{
    std::thread th_;
public:
    template <typename Callable, typename... Args,
                  typename = ::std::enable_if_t<!::std::is_same_v<::std::decay_t<Callable>, ThreadGuard>>>
    ThreadGuard(Callable&& callable, Args&&... args) :
        th_{std::forward<Callable>(callable), std::forward<Args>(args)...}
    {
    }

    ThreadGuard(const ThreadGuard&) = delete;
    ThreadGuard& operator=(const ThreadGuard&) = delete;
    ThreadGuard(ThreadGuard&&) = default;
    ThreadGuard& operator=(ThreadGuard&&) = default;

    ~ThreadGuard()
    {
        if (th_.joinable())
            th_.join();
    }
};

void say_hello(int id)
{
    cout << "Hello " << id << endl;
}

int main()
{
    thread th1(say_hello, 1);
    th1.join();
    ThreadGuard tg(say_hello, 2);
    return 0;
}
