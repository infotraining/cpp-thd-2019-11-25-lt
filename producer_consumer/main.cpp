#include <iostream>
#include <vector>
#include <mutex>
#include <thread>
#include <random>
#include <algorithm>
#include <atomic>
#include <condition_variable>
#include "../utils.hpp"

using namespace std;

class DataAtomic
{
    vector<int> data_;
    atomic<bool> is_ready = false;
public:
    void produce()
    {
        cout << "start preparing" << endl;
        data_.resize(1000);
        random_device rd;
        generate_n(data_.begin(), 1000, [&rd]{ return rd() % 100;});
        this_thread::sleep_for(5s);
        cout << "end preparing" << endl;

        is_ready.store(true);
    }

    void consume(int id)
    {
        while(!is_ready.load()) {
            this_thread::yield();
        }
        cout << id << " consume begins" << endl;
        long sum = accumulate(data_.begin(), data_.end(), 0L);
        cout << id << " sum = " << sum << endl;
    }
};

class Data
{
    vector<int> data_;
    bool is_ready = false;
    condition_variable cv_is_ready;
    mutex mtx;
public:
    void produce()
    {
        cout << "start preparing" << endl;
        data_.resize(1000);
        random_device rd;
        generate_n(data_.begin(), 1000, [&rd]{ return rd() % 100;});
        this_thread::sleep_for(5s);
        cout << "end preparing" << endl;
        {
            lock_guard l(mtx);
            is_ready = true;
        }
        cv_is_ready.notify_all();
    }

    void consume(int id)
    {
        //this_thread::sleep_for(1s);
        {
            unique_lock ul(mtx);
            cv_is_ready.wait(ul, [this] { return is_ready;});
        }
        cout << id << " consume begins" << endl;
        long sum = accumulate(data_.begin(), data_.end(), 0L);
        cout << id << " sum = " << sum << endl;
    }
};

int main()
{
    Data data;
    vector<ThreadGuard> thds;
    thds.emplace_back([&] { data.produce(); });
    thds.emplace_back([&] { data.consume(1); });
    thds.emplace_back([&] { data.consume(2); });
    return 0;
}
