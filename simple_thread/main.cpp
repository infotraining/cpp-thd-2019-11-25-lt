#include <iostream>
#include <thread>
#include <string>
#include <vector>
#include "../utils.hpp"

using namespace std;

void say_hello(int id)
{
    cout << "Hello from thread " << id << endl;
    this_thread::sleep_for(100ms);
}

void background_work(int id, const string& text)
{
    cout << "Background work " << id << endl;
    cout << text << endl;
}

thread create_thread(int id)
{
    thread res(say_hello, id);
    return res;
}

int main()
{
    cout << "Hello World!" << endl;
    thread th1(say_hello, 1);
    //thread th2 = th1;  // illegal, no copy ctor
    cout << "Before join" << endl;
    thread th2 = create_thread(2);

    th2.join();  // move from function
    thread th3(move(th1));
    //th1.join(); // can't do it on moved thread
    //th3.join();

    vector<thread> threads;
    threads.push_back(thread(say_hello, 3));
    threads.push_back(move(th3));
    threads.emplace_back(say_hello, 4);
    threads.push_back(create_thread(5));

    for(auto& th : threads)
        th.join();

    const string text = "Ala ma kota";
    thread th6(background_work, 1, cref(text));
    th6.join();

    cout << "After join" << endl;
    ThreadGuard thg(background_work, 2, "Ola ma psa");
    vector<ThreadGuard> thv;

    return 0;
}
