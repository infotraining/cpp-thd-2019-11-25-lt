#include <iostream>
#include <vector>
#include <shared_mutex>
#include <random>
#include "../utils.hpp"

using namespace std;

class ReaderWriter
{
    vector<ThreadGuard> thds_;
    std::array<int, 5> resource_{0,0,0,0,0};
    shared_mutex mtx;

    [[noreturn]] void Reader(int id)
    {
        random_device rd;
        mt19937 mt(rd());
        uniform_int_distribution<size_t> dist(0, 4);
        while(true)
        {
            size_t i = dist(mt);
            {
                shared_lock l(mtx);
                cout << id << " reading at " << i << " => " << resource_[i] << endl;
                //this_thread::sleep_for(500ms);
            }
            this_thread::yield();

        }
    }
    [[noreturn]] void Writer()
    {
        random_device rd;
        mt19937 mt(rd());
        uniform_int_distribution<size_t> dist(0, 4);
        while(true)
        {
            size_t i = dist(mt);
            int val = static_cast<int>(dist(mt));
            {
                unique_lock l(mtx);
                cout << "writing at " << i << " val = " << val << endl;
                resource_[i] = val;
                this_thread::sleep_for(1s);
            }
        }
    }
public:
    ReaderWriter()
    {
        thds_.emplace_back(&ReaderWriter::Reader, this, 1);
        thds_.emplace_back(&ReaderWriter::Reader, this, 2);
        thds_.emplace_back(&ReaderWriter::Reader, this, 3);
        thds_.emplace_back(&ReaderWriter::Reader, this, 4);
        thds_.emplace_back(&ReaderWriter::Writer, this);
    }
};

int main()
{
    ReaderWriter rw{};
    return 0;
}
