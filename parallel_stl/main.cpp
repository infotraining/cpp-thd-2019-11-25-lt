#include <iostream>
#include <algorithm>
#include <random>
#include <vector>
#include <execution>
#include <celero/Celero.h>

using namespace std;
CELERO_MAIN

constexpr int n = 5;
constexpr int it = 20;
constexpr long cnt = 100'000;

BASELINE(STL, SingleThread, n, it)
{
    auto seed = random_device{}();
    vector<double> data(cnt);
    generate(data.begin(), data.end(), mt19937_64{seed});
    sort(data.begin(), data.end());
    is_sorted(data.begin(), data.end());
}

BENCHMARK(STL, parallel, n, it)
{
    auto seed = random_device{}();
    vector<double> data(cnt);
    generate(std::execution::par, data.begin(), data.end(), mt19937_64{seed});
    sort(std::execution::par, data.begin(), data.end());
    is_sorted(std::execution::par, data.begin(), data.end());
}

BENCHMARK(STL, unseq, n, it)
{
    auto seed = random_device{}();
    vector<double> data(cnt);
    generate(std::execution::unseq, data.begin(), data.end(), mt19937_64{seed});
    sort(std::execution::unseq, data.begin(), data.end());
    is_sorted(std::execution::unseq, data.begin(), data.end());
}

BENCHMARK(STL, parallel_unseq, n, it)
{
    auto seed = random_device{}();
    vector<double> data(cnt);
    generate(std::execution::par_unseq, data.begin(), data.end(), mt19937_64{seed});
    sort(std::execution::par_unseq, data.begin(), data.end());
    is_sorted(std::execution::par_unseq, data.begin(), data.end());
}

