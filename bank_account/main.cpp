#include <iostream>
#include <vector>
#include <mutex>
#include "../utils.hpp"

using namespace std;

class BankAccount
{
    mutex mtx_;
    const int id_;
    double balance_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {}

    void print()
    {
        cout << "id: " << id_ << " balance " << balance_ << endl;
    }

    void deposit(double amount)
    {
        lock_guard lg(mtx_);
        balance_ += amount;
    }

    void transfer(BankAccount& to, double amount)
    {
        // since c++11
//        unique_lock lg1(mtx_, defer_lock);
//        unique_lock lg2(to.mtx_, defer_lock);
//        lock(lg1, lg2);
        // since c++17
        scoped_lock sl(mtx_, to.mtx_);
        balance_ -= amount;
        to.balance_ += amount;
    }
};

void make_deposits(BankAccount& ba, int n_of_op, double amount)
{
    for (int i = 0 ; i < n_of_op ; ++i)
        ba.deposit(amount);
}


void make_transfers(BankAccount& ba1, BankAccount& ba2, int n_of_op, double amount)
{
    for (int i = 0 ; i < n_of_op ; ++i)
        ba1.transfer(ba2, amount);
}

int main()
{
    BankAccount ba1(1, 1000);
    BankAccount ba2(2, 5000);
    {
        vector<ThreadGuard> thds;
        //thds.emplace_back(make_deposits, ref(ba1), 1000, 10);
        thds.emplace_back(make_transfers, ref(ba1), ref(ba2), 100, 10);
        thds.emplace_back(make_transfers, ref(ba2), ref(ba1), 100, 10);
    }
    ba1.print();
    ba2.print();
    return 0;
}
