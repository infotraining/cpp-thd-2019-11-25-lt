#include <iostream>
#include <thread>
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;

atomic<long> counter = 0;
mutex mtx;
long counter_mtx = 0;

void increase_counter()
{
    for (size_t i = 0 ; i < 1'000'000 ; ++i)
        ++counter;
}

void increase_counter_mutex()
{
    for (size_t i = 0 ; i < 1'000'000 ; ++i)
    {
        //mtx.lock();
        lock_guard lg(mtx);
        ++counter_mtx;
        //mtx.unlock();
    }
}

int main()
{
    vector<thread> thds;
    cout << "Counter " << counter << endl;
    for (int i = 0 ; i < 8 ; ++i)
        thds.emplace_back(increase_counter_mutex);
    for (auto& th : thds) th.join();
    cout << "Counter after " << counter_mtx << endl;
    cout << "is lock free?" << counter.is_lock_free() << endl;
    return 0;
}
